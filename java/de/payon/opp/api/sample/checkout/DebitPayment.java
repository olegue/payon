package de.payon.opp.api.sample.checkout;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import de.payon.opp.api.sample.bindings.BasicResponse;
import de.payon.opp.api.sample.bindings.CheckoutResponse;

/**
 * 
 * @author odavidyuk 
 * 
 * The DebitPayment interface calls the OPP API in order to prepare a checkout and get the payment status.
 *
 */
public interface DebitPayment {
	
	public HttpPost buildCheckoutRequest(String queryUrl);
	
	public CheckoutResponse executeCheckoutRequest(HttpPost request);
	
	public HttpGet buildStatusQuery (String checkoutId, String queryUrl);
	
	public BasicResponse executeStatusQuery(HttpGet request);
}
