package de.payon.opp.api.sample.checkout;

import static de.payon.opp.api.sample.utils.LoggerWrapper.log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.payon.opp.api.sample.bindings.BasicResponse;
import de.payon.opp.api.sample.bindings.CheckoutResponse;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DebitPaymentImpl implements DebitPayment {
	
	private static final ObjectMapper objectMapper = new ObjectMapper();
	
	public HttpPost buildCheckoutRequest(String queryUrl) {
		
		//create the request object
		HttpPost request = new HttpPost(queryUrl);
				
		//add the request headers
		request.addHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
		
		//encode request params
		ArrayList<NameValuePair> requestParameters;
		requestParameters = new ArrayList<NameValuePair>();
		requestParameters.add(new BasicNameValuePair("authentication.userId", "8a8294174b7ecb28014b9699220015cc"));
		requestParameters.add(new BasicNameValuePair("authentication.password", "sy6KJsT8"));
		requestParameters.add(new BasicNameValuePair("authentication.entityId", "8a8294174b7ecb28014b9699220015ca"));
		requestParameters.add(new BasicNameValuePair("amount", "11.97"));
		requestParameters.add(new BasicNameValuePair("currency", "EUR"));
		requestParameters.add(new BasicNameValuePair("paymentBrand", "VISA"));
		requestParameters.add(new BasicNameValuePair("paymentType", "DB"));
		requestParameters.add(new BasicNameValuePair("card.number", "4200000000000000"));
		requestParameters.add(new BasicNameValuePair("card.holder", "Jane Jones"));
		requestParameters.add(new BasicNameValuePair("card.expiryMonth", "05"));
		requestParameters.add(new BasicNameValuePair("card.expiryYear", "2018"));
		requestParameters.add(new BasicNameValuePair("card.cvv", "123"));
	    
	    try {
	    	request.setEntity(new UrlEncodedFormEntity(requestParameters, "utf-8"));
		} catch (UnsupportedEncodingException e) {
			log("Error encoding post parameters: {}", e.getMessage());
			//ignore
		}
		return request;
	}
	
    public CheckoutResponse executeCheckoutRequest(HttpPost request) {
    	
    	CloseableHttpResponse response = null;
    	CheckoutResponse paymentResponse = null;

    	try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
    		
    		log("Request {}", request);
    		response = httpClient.execute(request);
			
    		HttpEntity entity = response.getEntity();
    		StatusLine httpStatusLine = response.getStatusLine();
			
    		String responseAsString = EntityUtils.toString(entity);
    		log("Received HTTP Status {}", httpStatusLine);
    		log("Response {}", responseAsString);
			
    		paymentResponse = objectMapper.readValue(responseAsString, CheckoutResponse.class);
			
    	} catch (ClientProtocolException e) {
    		log("Error: {}", e.getMessage());
    		//handle
    	} catch (IOException e) {
    		log("Error: {}", e.getMessage());
    		//handle
    	} finally {
    		IOUtils.closeQuietly(response);
    	}
    	return paymentResponse; 
    }
    
    public HttpGet buildStatusQuery (String checkoutId, String queryUrl) {		

    	//build the request URL 
    	StringBuffer requestUrl = new StringBuffer();
    	requestUrl.append(queryUrl);
    	requestUrl.append("/");
    	requestUrl.append(checkoutId);
    	requestUrl.append("/");
    	requestUrl.append("payment");
    	
    	//create the request object
    	HttpGet request = new HttpGet(requestUrl.toString());
    	
    	//add the request headers
    	request.addHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
		
    	log("GET Request: {}", request.toString());
    	return request;
	}
	
    public BasicResponse executeStatusQuery(HttpGet request) {

    	BasicResponse queryStatusResponse = null;
		
    	try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

    		HttpResponse httpResponse = httpClient.execute(request);	
    		HttpEntity entity = httpResponse.getEntity();
    		String responseAsString = EntityUtils.toString(entity);
    		log("Received GET Reponse: {}", responseAsString);
    		queryStatusResponse = objectMapper.readValue(responseAsString, BasicResponse.class);

    	} catch (IOException e) {
    		log("ERROR: {}", e.getMessage());
    		// handle
    	}
    	return queryStatusResponse;
    }
   
}
