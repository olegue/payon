package de.payon.opp.api.sample.bindings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Data @ToString(includeFieldNames=true) @JsonIgnoreProperties(ignoreUnknown = true)
public class Card {
	private String bin;
	private String last4Digits;
	private String holder;
	private String expiryMonth;
	private String expiryYear;
}
