package de.payon.opp.api.sample.bindings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data @EqualsAndHashCode(callSuper=true) @ToString(includeFieldNames=true) @JsonIgnoreProperties(ignoreUnknown = true)
public class CheckoutResponse extends BasicResponse {
	private String descriptor;
	private Risk risk;
	private String buildNumber;
	private String timestamp;
	private String ndc;
	private String id;
	
}
