package de.payon.opp.api.sample.bindings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Data @ToString(includeFieldNames=true) @JsonIgnoreProperties(ignoreUnknown = true)
public class BasicResponse {
	private String paymentType; 
	private String paymentBrand; 
	private String amount; 
	private String currency; 
	private Result result; 
	private Card card; 
}
