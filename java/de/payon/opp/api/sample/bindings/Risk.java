package de.payon.opp.api.sample.bindings;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Data @ToString(includeFieldNames=true) @JsonIgnoreProperties(ignoreUnknown = true)
public class Risk {
	private String score;
}
