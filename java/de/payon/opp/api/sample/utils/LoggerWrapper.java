package de.payon.opp.api.sample.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LoggerWrapper {
	private static Logger logger = LoggerFactory.getLogger("DebitPayment");
	
	public static void log(String msg, Object obj) {
		logger.debug(msg, obj);
	}
	
	public static void debug(Class<?> caller, String msg) {
		LoggerFactory.getLogger(caller.getName()).debug(msg);
	}

}
