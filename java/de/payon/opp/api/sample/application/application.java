package de.payon.opp.api.sample.application;

import static de.payon.opp.api.sample.utils.LoggerWrapper.log;

import de.payon.opp.api.sample.bindings.CheckoutResponse;
import de.payon.opp.api.sample.checkout.DebitPaymentImpl;
import de.payon.opp.api.sample.bindings.BasicResponse;

public class application {
	public static void main(String[] args) {
		
		String baseUrl = "https://test.oppwa.com/v1/checkouts";
				
		DebitPaymentImpl testPayment = new DebitPaymentImpl();
		CheckoutResponse checkoutResponse = testPayment.executeCheckoutRequest(testPayment.buildCheckoutRequest(baseUrl));
		
		String id = checkoutResponse.getId();
		log("Checkout Id: {}", id);
		
		BasicResponse queryResponse = testPayment.executeStatusQuery(testPayment.buildStatusQuery(id, baseUrl));
		log("Checkout Status: {}", queryResponse.getResult().getDescription());
	}

}
